"""Python Assignment 1"""


def star_pyramid(rows):
    """ Print star pyramid pattern"""

    space = rows-1
    star = 1
    for i in range(0, rows):
        for j in range(0, space):
            print(end=" ")
        space = space - 1
        for k in range(0, star):
            print("*", end="")
        star = star + 2
        print("\r")


def inverted_star_pyramid(rows):
    """ Print inverted star pyramid pattern"""

    space = 0
    star = rows * 2 - 1
    for i in range(0, rows):
        for j in range(0, space):
            print(end=" ")
        space = space + 1
        for k in range(0, star):
            print("*", end="")
        star = star - 2
        print("\r")


def half_star_pyramid(rows):
    """ Print half star pyramid pattern"""

    for i in range(0, rows):
        for j in range(0, i + 1):
            print("* ", end="")
        print("\r")

        
def inverted_half_star_pyramid(rows):
    """" print inverted half star pyramid pattern"""

    stars = rows
    for i in range(0, rows):
        for j in range(0, stars):
            print("* ", end="")
        stars = stars - 1
        print("\r")


def inverted_hollow_star_pyramid(rows):
    """ Print inverted hollow star pyramid pattern"""

    space = 0
    star = rows * 2 - 1
    for i in range(0, rows):
        for j in range(0, space):
            print(end=" ")
        space = space + 1
        for k in range(0, star):
            if i == 0:
                print("*", end="")
            elif k == 0 or k == star - 1:
                print("*", end="")
            else:
                print(" ", end="")
        star = star - 2
        print("\r")


def hollow_star_pyramid(rows):
    """ Print hollow star pyramid pattern"""

    space = rows - 1
    star = 1
    for i in range(0, rows):
        for j in range(0, space):
            print(end=" ")
        space = space - 1
        for k in range(0, star):
            if i == rows - 1:
                print("*", end="")
            elif k == 0 or k == star - 1:
                print("*", end="")
            else:
                print(" ", end="")

        star = star + 2
        print("\r")


def print_menu():
    """Print main menu"""

    print('-' * 180)
    print("Press 1. Task1  Full star pyramid pattern")
    print("Press 2. Task2  Inverted full star pyramid pattern")
    print("Press 3. Task3  Half star pyramid pattern")
    print("Press 4. Task4  Inverted half star pyramid pattern")
    print("Press 5. Task5  Hollow star pyramid pattern")
    print("Press 6. Task6  Inverted hollow star pyramid pattern")
    print("Press 7. Exit ")
    print('-' * 180)


def take_input():
    """Take user input """

    total_rows = int(input("Enter no of rows:"))
    if total_rows > 0:
        return total_rows
    else:
        print("Wrong Input")
        exit()


if __name__ == '__main__':
    """Main code starts from here"""

    loop = True
    while loop:
        print_menu()
        choice = int(input("Enter your choice"))
        if choice == 1:
            rows = take_input()
            print("Star Pyramid")
            star_pyramid(rows)
        elif choice == 2:
            rows = take_input()
            print("Inverted Star Pyramid")
            inverted_star_pyramid(rows)
        elif choice == 3:
            rows = take_input()
            print(" Half Star Pyramid")
            half_star_pyramid(rows)
        elif choice == 4:
            rows = take_input()
            print("Inverted Half Star Pyramid")
            inverted_half_star_pyramid(rows)
        elif choice == 5:
            rows = take_input()
            print("Hollow Star Pyramid")
            hollow_star_pyramid(rows)
        elif choice == 6:
            rows = take_input()
            print("Inverted Hollow Star Pyramid")
            inverted_hollow_star_pyramid(rows)
        elif choice == 7:
            loop = False
        else:
            print("Wrong option selection. Enter any key to try again..")
        print("\r")
